# jsBetterLog 1.0 #

A library for better logging. 

- Multiple log types with different styling
- Change the styles of each log type
- Display timestamps
- Turn logging on/off

### Setup ###

Include the js file in your project folder and link it in the html. All done!

### CONTACT ###

E-mail: matis.lepik@gmail.com
Skype: matis.lepik