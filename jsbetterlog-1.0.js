/*jslint nomen:true */
/*global console,window,document */

(function () {

    var L = {

        timestamps: false,
        timestampStyle: "font-style:italic;font-size:0.8em;",

        log: function (input) {
            this._log(this._doLog, input, this._cssLog);
        },

        error: function (input) {
            this._log(this._doError, input, this._cssError);
        },

        warn: function (input) {
            this._log(this._doWarn, input, this._cssWarn);
        },

        logcss: function (input, css) {
            this._log(this._doLogcss, input, css);
        },
        pure: function (input) {
            if (this._doPure) {
                console.log(input);
            }
        },

        /**
         * Set css for log type.
         * @param {String} type - the log type you want to change (log/error/warn)
         * @param {String} css - new css code
         * @return {boolean} success - whether the css was successfully set or not
         */
        setCss: function (type, css) {
            switch (type.toLowerCase()) {
            case "log":
                this._cssLog = css;
                return true;
            case "error":
                this._cssError = css;
                return true;
            case "warn":
                this._cssWarn = css;
                return true;
            default:
                this.error("Invalid log type");
                return false;
            }
        },

        /**
         * Shortcut function to set colour for log type. This will clear all other styles
         * @param {String} type
         * @param {String} color
         * @return {boolean} success
         */
        setColor: function (type, color) {
            switch (type.toLowerCase()) {
            case "log":
                this._cssLog = 'color:' + color;
                return true;
            case "error":
                this._cssError = 'color:' + color;
                return true;
            case "warn":
                this._cssWarn = 'color:' + color;
                return true;
            default:
                this.error("Invalid log type");
                return false;
            }
        },

        /**
         * Activate/deactivate logging on types
         * Accepts "all" as type to set all
         * @param {String} type
         * @param {boolean} log
         * @return {boolean} success
         */
        setLogging: function (type, log) {
            switch (type.toLowerCase()) {
            case "log":
                this._doLog = log;
                return true;
            case "error":
                this._doError = log;
                return true;
            case "warn":
                this._doWarn = log;
                return true;
            case "logcss":
                this._doLogcss = log;
                return true;
            case "pure":
                this._doPure = log;
            case "all":
                this._doLog = log;
                this._doError = log;
                this._doWarn = log;
                this._doLogcss = log;
                this._doPure = log;
                return true;
            default:
                this.error("Invalid log type");
                return false;
            }
        },


        /* "Private" properties, shouldn't have to use manually */

        /**
         * Basic log function
         * @param{boolean} isActive
         * @param{String} input
         * @param{String} css
         */
        _log: function (isActive, input, css) {
            if (isActive && input !== undefined) {
                if (this.timestamps) {
                    console.log('%c' + this._getTime() + ":%c " + input, this.timestampStyle, css);
                } else {
                    console.log('%c' + input, css);
                }
            }
        },
        _getTime: function () {
            var date = new Date();
            return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + ":" + date.getMilliseconds();
        },
        _cssLog: '',
        _cssError: 'color:#b00000',
        _cssWarn: 'color:#e27500',
        _doLog: true,
        _doError: true,
        _doWarn: true,
        _doLogcss: true,
        _doPure: true
    };

    window.L = window.jsBetterLog = L;
}());